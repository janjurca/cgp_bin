import cv2
import glob

for filename in glob.glob("color/*.png"):
    image = cv2.imread(filename)
    image = cv2.resize(image,(300,300))
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    sobely = cv2.Sobel(gray, cv2.CV_64F, 0, 1, ksize=3)

    #cv2.imshow('Original image', image)
    #cv2.imshow('Gray image', gray)
    #cv2.imshow('Sobel image', sobely)

    cv2.imwrite('bw/'+ filename.split("/")[-1],gray)
    cv2.imwrite('sobel/'+ filename.split("/")[-1],sobely)

    cv2.waitKey(0)
cv2.destroyAllWindows()
