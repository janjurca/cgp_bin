void write_data(){
    Mat output(298, 298, CV_8U, Scalar(0));

    int row = 0;
    int col = 0;
    for (size_t i = 0; i < param_fitev; i++) {
        output.at<uchar>(row,col) = dataset[i].label;
        col++;
        if (col == 298) {
            col = 0;
            row++;
        }
    }
    imwrite("sobel_reversed.png", output);

    Mat output2(300, 300, CV_8U, Scalar(0));

    row = 0;
    col = 0;
    for (size_t i = 0; i < param_fitev; i += 3) {
        row = (int) i/(IMAGEWIDTH-2);
        col = i % (IMAGEWIDTH-2);
        output2.at<uchar>(row,col) = dataset[i].input[0];
        output2.at<uchar>(row,col+1) = dataset[i].input[1];
        output2.at<uchar>(row,col+2) = dataset[i].input[2];

    }
    imwrite("orig_reversed.png", output2);

}
