#ifndef COMPILER_H
#define COMPILER_H value

#include "cgp.h"

// typedef int nodetype;
// nodetype* nodeoutput;

void cgp_init_consts() {}

#define MAXCODESIZE 27
typedef void evalfunc(void);
unsigned char* code[MAX_POPSIZE];
void cgp_compile(unsigned char* pcode, chromosome_t p_chrom, int* isused)
{
    #define C(val)  *(pcode++)=(val)
    #define CI(val) {*((uint32_t *)(pcode))= (uint32_t)((__PTRDIFF_TYPE__)val); pcode += sizeof(uint32_t);}
    #define CL(val) {*((uint64_t *)(pcode))= (uint64_t)((__PTRDIFF_TYPE__)val); pcode += sizeof(uint64_t);}

    int pnodeout = 0;
    int in1,in2,fce;
    int out = PARAM_IN - 1;

    /// Save modified registers
    /// %rbx, %edx, %eax
    C(0x53);                                          //push   %rbx
    C(0x52);                                          //push   %rdx
    /// Load nodeoutputs -> rbx
    C(0x48);C(0xbb);CL(nodeoutput);
    /// Native code generation
    for (int i=0; i < PARAM_C; i++)
        for (int j=0; j < PARAM_R; j++)
        {
            in1 = *p_chrom++; in2 = *p_chrom++; fce = *p_chrom++; out++;
            #ifdef DONOTEVALUATEUNUSEDNODES
            if (!isused[out]) continue;
            #endif
            switch (fce)
            {
              case 0:
                  ///nodeoutput[out] = 255;
                  C(0xc7);C(0x83);CI(4*out);C(0xff);C(0x00);C(0x00);C(0x00);//0/0 movl   $0xff,out(%rbx)
                  //code size: 10, stack requirements: 0
                  //registers: %rbx
                  break;
              case 1:
                  ///nodeoutput[out] = nodeoutput[in1];
                  C(0x8b);C(0x93);CI(4*in1);                        //0/0 mov    in1(%rbx),%edx
                  C(0x89);C(0x93);CI(4*out);                        //6/6 mov    %edx,out(%rbx)
                  //code size: 12, stack requirements: 0
                  //registers: %edx, %rbx
                  break;
              case 2:
                  ///nodeoutput[out] = 255-nodeoutput[in1];
                  C(0xb8);C(0xff);C(0x00);C(0x00);C(0x00);          //0/0 mov    $0xff,%eax
                  C(0x2b);C(0x83);CI(4*in1);                        //5/5 sub    in1(%rbx),%eax
                  C(0x89);C(0x83);CI(4*out);                        //b/b mov    %eax,out(%rbx)
                  //code size: 17, stack requirements: 0
                  //registers: %eax, %rbx
                  break;
              case 3:
                  ///nodeoutput[out] = nodeoutput[in1] | nodeoutput[in2];
                  C(0x8b);C(0x93);CI(4*in1);                        //0/0 mov    in1(%rbx),%edx
                  C(0x0b);C(0x93);CI(4*in2);                        //6/6 or     in2(%rbx),%edx
                  C(0x89);C(0x93);CI(4*out);                        //c/c mov    %edx,out(%rbx)
                  //code size: 18, stack requirements: 0
                  //registers: %edx, %rbx
                  break;
              case 4:
                  ///nodeoutput[out] = (~nodeoutput[in1]) | nodeoutput[in2];
                  C(0x8b);C(0x83);CI(4*in1);                        //0/0 mov    in1(%rbx),%eax
                  C(0xf7);C(0xd0);                                  //6/6 not    %eax
                  C(0x0b);C(0x83);CI(4*in2);                        //8/8 or     in2(%rbx),%eax
                  C(0x89);C(0x83);CI(4*out);                        //e/e mov    %eax,out(%rbx)
                  //code size: 20, stack requirements: 0
                  //registers: %eax, %rbx
                  break;
              case 5:
                  ///nodeoutput[out] = ~(nodeoutput[in1] & nodeoutput[in2]);
                  C(0x8b);C(0x83);CI(4*in1);                        //0/0 mov    in1(%rbx),%eax
                  C(0x23);C(0x83);CI(4*in2);                        //6/6 and    in2(%rbx),%eax
                  C(0xf7);C(0xd0);                                  //c/c not    %eax
                  C(0x89);C(0x83);CI(4*out);                        //e/e mov    %eax,out(%rbx)
                  //code size: 20, stack requirements: 0
                  //registers: %eax, %rbx
                  break;
              case 6:
                  ///nodeoutput[out] = (nodeoutput[in1] & nodeoutput[in2]);
                  C(0x8b);C(0x93);CI(4*in1);                        //0/0 mov    in1(%rbx),%edx
                  C(0x23);C(0x93);CI(4*in2);                        //6/6 and    in2(%rbx),%edx
                  C(0x89);C(0x93);CI(4*out);                        //c/c mov    %edx,out(%rbx)
                  //code size: 18, stack requirements: 0
                  //registers: %edx, %rbx
                  break;
              case 7:
                  ///nodeoutput[out] = (nodeoutput[in1] ^ nodeoutput[in2]);
                  C(0x8b);C(0x93);CI(4*in1);                        //0/0 mov    in1(%rbx),%edx
                  C(0x33);C(0x93);CI(4*in2);                        //6/6 xor    in2(%rbx),%edx
                  C(0x89);C(0x93);CI(4*out);                        //c/c mov    %edx,out(%rbx)
                  //code size: 18, stack requirements: 0
                  //registers: %edx, %rbx
                  break;
              case 8:
                  ///nodeoutput[out] = nodeoutput[in1] >> 1;
                  C(0x8b);C(0x83);CI(4*in1);                        //0/0 mov    in1(%rbx),%eax
                  C(0xd1);C(0xf8);                                  //6/6 sar    %eax
                  C(0x89);C(0x83);CI(4*out);                        //8/8 mov    %eax,out(%rbx)
                  //code size: 14, stack requirements: 0
                  //registers: %eax, %rbx
                  break;
              case 9:
                  ///nodeoutput[out] = nodeoutput[in1] >> 2;
                  C(0x8b);C(0x83);CI(4*in1);                        //0/0 mov    in1(%rbx),%eax
                  C(0xc1);C(0xf8);C(0x02);                          //6/6 sar    $0x2,%eax
                  C(0x89);C(0x83);CI(4*out);                        //9/9 mov    %eax,out(%rbx)
                  //code size: 15, stack requirements: 0
                  //registers: %eax, %rbx
                  break;
              case 10:
                  ///nodeoutput[out] = nodeoutput[in1] + nodeoutput[in2];
                  C(0x8b);C(0x93);CI(4*in2);                        //0/0 mov    in2(%rbx),%edx
                  C(0x03);C(0x93);CI(4*in1);                        //6/6 add    in1(%rbx),%edx
                  C(0x89);C(0x93);CI(4*out);                        //c/c mov    %edx,out(%rbx)
                  //code size: 18, stack requirements: 0
                  //registers: %edx, %rbx
                  break;
              case 11:
                  ///nodeoutput[out] = (nodeoutput[in1] + nodeoutput[in2])/2;
                  C(0x8b);C(0x83);CI(4*in2);                        //0/0 mov    in2(%rbx),%eax
                  C(0x03);C(0x83);CI(4*in1);                        //6/6 add    in1(%rbx),%eax
                  C(0x89);C(0xc2);                                  //c/c mov    %eax,%edx
                  C(0xc1);C(0xe8);C(0x1f);                          //e/e shr    $0x1f,%eax
                  C(0x01);C(0xd0);                                  //11/11 add    %edx,%eax
                  C(0xd1);C(0xf8);                                  //13/13 sar    %eax
                  C(0x89);C(0x83);CI(4*out);                        //15/15 mov    %eax,out(%rbx)
                  //code size: 27, stack requirements: 0
                  //registers: %eax, %edx, %rbx
                  break;
              case 12:
                  ///nodeoutput[out] = (nodeoutput[in1] > nodeoutput[in2])?nodeoutput[in1] : nodeoutput[in2];
                  C(0x8b);C(0x93);CI(4*in1);                        //0/0 mov    in1(%rbx),%edx
                  C(0x39);C(0x93);CI(4*in2);                        //6/6 cmp    %edx,in2(%rbx)
                  C(0x0f);C(0x4d);C(0x93);CI(4*in2);                //c/c cmovge in2(%rbx),%edx
                  C(0x89);C(0x93);CI(4*out);                        //13/13 mov    %edx,out(%rbx)
                  //code size: 25, stack requirements: 0
                  //registers: %edx, %rbx
                  break;
              case 13:
                  ///nodeoutput[out] = (nodeoutput[in1] > nodeoutput[in2])?nodeoutput[in1] : nodeoutput[in2];
                  C(0x8b);C(0x93);CI(4*in1);                        //0/0 mov    in1(%rbx),%edx
                  C(0x39);C(0x93);CI(4*in2);                        //6/6 cmp    %edx,in2(%rbx)
                  C(0x0f);C(0x4d);C(0x93);CI(4*in2);                //c/c cmovge in2(%rbx),%edx
                  C(0x89);C(0x93);CI(4*out);                        //13/13 mov    %edx,out(%rbx)
                  //code size: 25, stack requirements: 0
                  //registers: %edx, %rbx
                  break;
              default:
                  abort();
              }
        }
    /// Restore modified registers
    C(0x5a);                                          //pop    %rdx
    C(0x5b);                                          //pop    %rbx
    /// Return
    C(0xc3);
}

#if !(defined __LP64__ || defined __LLP64__) || defined _WIN32 && !defined _WIN64
#error "32-bit system is not supported"
#else
// we are compiling for a 64-bit system
#endif

// This file was generated at: http://www.fit.vutbr.cz/~vasicek/cgp Token:G1-Ri36oF7 GCC:gcc-7.3
// Vasicek, Z., Slany, K.: Efficient Phenotype Evaluation in Cartesian Genetic Programming,
// Proc. of the 15th European Conference on Genetic Programming, EuroGP 2012

#endif
